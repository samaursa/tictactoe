# README #

TicTacToe game using RakNet.

### Can I use this code for Assignment1? ###

Yes you can but keep in mind the assignment requirements!

### How do I get set up? ###

General the project using CMake. It's probably a good idea to generate two projects **build_2013_server** and **build_2013_client**.

### Still have some questions? ###

Feel free to email me. However, make sure you try your best to understand the code and search RakNet documentation for answers!