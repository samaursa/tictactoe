#include "RakPeer.h"
#include "RakSleep.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "MessageIdentifiers.h"
#include <stdio.h>
#include <iostream>

namespace tic_tac_toe {

  namespace messages {
    enum
    {
      ID_TTT_TURN_AND_BOARD = ID_USER_PACKET_ENUM,
      ID_TTT_OTHER_USER_WON
    }; unsigned char value_type;
  };

#pragma pack(push, 1)
  struct Board
  {
    enum { EMTPY = 0, CIRCLE, CROSS }; typedef char  move_type;

    bool IsEmtpy(int a_index)
    { return m_cells[a_index] == EMTPY; }

    move_type& operator[](int a_index)
    { return m_cells[a_index]; }

    unsigned char m_packetID;
    move_type     m_cells[9];
  };
#pragma pack(pop)

  Board::move_type  g_mySymbol = Board::EMTPY;

  void PrintBoard(Board& b)
  {
    printf("\n");
    for (int i = 0, counter = 0; i < 9; ++i, ++counter)
    {
      if (counter > 2)
      { 
        printf("\n------------\n"); counter = 0;
      }

      if (b[i] == Board::EMTPY) { printf(" %d ", i); }
      else if (b[i] == Board::CIRCLE) { printf(" O "); }
      else if (b[i] == Board::CROSS) { printf(" X "); }

      if (counter != 2)
      { printf("|"); }
    }
  }

  bool CheckWin(Board& b)
  {
    if ( (b[0] == g_mySymbol && b[1] == g_mySymbol && b[2] == g_mySymbol) ||
         (b[3] == g_mySymbol && b[4] == g_mySymbol && b[5] == g_mySymbol) ||
         (b[6] == g_mySymbol && b[7] == g_mySymbol && b[8] == g_mySymbol) ||

         (b[0] == g_mySymbol && b[3] == g_mySymbol && b[6] == g_mySymbol) ||
         (b[1] == g_mySymbol && b[4] == g_mySymbol && b[7] == g_mySymbol) ||
         (b[2] == g_mySymbol && b[5] == g_mySymbol && b[8] == g_mySymbol) ||

         (b[0] == g_mySymbol && b[4] == g_mySymbol && b[8] == g_mySymbol) ||
         (b[2] == g_mySymbol && b[4] == g_mySymbol && b[6] == g_mySymbol) )
    { return true; }

    return false;
  }

  unsigned char
    ProcessTurn(Board& b)
  {
    PrintBoard(b);

    int cell;
    do 
    {
      std::cout << "\nSelect an EMPTY cell: ";
      std::cin >> cell;
    } while (b.IsEmtpy(cell) == false);

    b[cell] = g_mySymbol;

    if (CheckWin(b)) { return messages::ID_TTT_OTHER_USER_WON; }
    else { return messages::ID_TTT_TURN_AND_BOARD; }
  }
};

unsigned char GetPacketIdentifier(RakNet::Packet *p)
{
  if (p == 0)
    return 255;

  if ((unsigned char) p->data[0] == ID_TIMESTAMP)
  {
    RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
    return (unsigned char) p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
  }
  else
    return (unsigned char) p->data[0];
}

bool LaunchServer(RakNet::RakPeerInterface* a_server, int a_port)
{
  a_server->SetIncomingPassword(0, 0);

  // IPV4 socket
  RakNet::SocketDescriptor  sd;
  sd.port = a_port;
  sd.socketFamily = AF_INET;

  if (a_server->Startup(4, &sd, 1) != RakNet::RAKNET_STARTED)
  {
    printf("\nFailed to start server with IPV4 ports");
    return false;
  }

  a_server->SetOccasionalPing(true);
  a_server->SetUnreliableTimeout(1000);
  a_server->SetMaximumIncomingConnections(4);

  printf("\nSERVER IP addresses:");
  for (unsigned int i = 0; i < a_server->GetNumberOfAddresses(); i++)
  {
    RakNet::SystemAddress sa = a_server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
    printf("\n%i. %s (LAN=%i)", i + 1, sa.ToString(false), sa.IsLANAddress());
  }

  return true;
}

bool LaunchClient(RakNet::RakPeerInterface* a_client, const char* a_serverIP,
                  int a_serverPort, int a_clientPort)
{
  // IPV4 socket
  RakNet::SocketDescriptor sd(a_clientPort, 0);
  sd.socketFamily = AF_INET;

  a_client->Startup(8, &sd, 1);
  a_client->SetOccasionalPing(true);

  if (a_client->Connect(a_serverIP, a_serverPort, 0, 0) != 
      RakNet::CONNECTION_ATTEMPT_STARTED)
  {
  printf("\nAttempt to connect to server FAILED");
    return false;
  }

  printf("\nCLIENT IP addresses:");
  for (unsigned int i = 0; i < a_client->GetNumberOfAddresses(); i++)
  { printf("\n%i. %s\n", i + 1, a_client->GetLocalIP(i)); }

  return true;
}

void PrintUsage()
{
  printf("\n-- USAGE --");
  printf("\nTicTacToe <serverPort>");
  printf("\nTicTacToe <serverIP> <serverPort> [clientPort]");
}

// both client and server handle packets the same way
bool HandlePackets(RakNet::RakPeerInterface* a_peer)
{
  for (RakNet::Packet* p = a_peer->Receive(); 
       p; 
       a_peer->DeallocatePacket(p), p = a_peer->Receive())
  {
    auto packetID = GetPacketIdentifier(p);

    switch(packetID)
    {
      // Handle server packets - of course, we should handle more types of 
      // packets for a more robust server
      case ID_NEW_INCOMING_CONNECTION:
      {
        printf("\nNew incoming connection from %s with GUID %s", 
               p->systemAddress.ToString(true), p->guid.ToString());
        
        // server takes CROSS
        tic_tac_toe::g_mySymbol = tic_tac_toe::Board::CROSS;
        break;
      }

      // Handle client packets
      case ID_ALREADY_CONNECTED:
      { printf("\nAlready connected with guid %s", p->guid); break; }
      case ID_CONNECTION_BANNED:
      { printf("\nWe are banned from this server"); return false; }
      case ID_CONNECTION_ATTEMPT_FAILED:
      { printf("\nConnection to server failed"); return false; }
      case ID_INVALID_PASSWORD:
      { printf("\nPassword invalid"); return false; }
      case ID_CONNECTION_REQUEST_ACCEPTED:
      {
        // client takes CIRCLE
        tic_tac_toe::g_mySymbol = tic_tac_toe::Board::CIRCLE;
        printf("Connection require ACCEPTED to %s with GUID %s\n", 
               p->systemAddress.ToString(true), p->guid.ToString());
        printf("My external address is %s\n", 
               a_peer->GetExternalID(p->systemAddress).ToString(true));

        tic_tac_toe::Board b;
        b.m_packetID = (unsigned char)tic_tac_toe::messages::ID_TTT_TURN_AND_BOARD;
        for (int i = 0; i < 9; ++i) { b[i] = tic_tac_toe::Board::EMTPY; }

        a_peer->Send((const char*)&b, sizeof(b), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

        break;
      }

      // handle common messages
      case ID_CONNECTION_LOST:
      { printf("\nConnection lost from %s", p->systemAddress.ToString(true)); break; }

      case tic_tac_toe::messages::ID_TTT_TURN_AND_BOARD:
      {
        tic_tac_toe::Board b = *(tic_tac_toe::Board*)p->data;

        auto msg = tic_tac_toe::ProcessTurn(b);

        if (msg == tic_tac_toe::messages::ID_TTT_OTHER_USER_WON)
        { 
          b.m_packetID = (unsigned char) tic_tac_toe::messages::ID_TTT_OTHER_USER_WON;
          a_peer->Send((const char*)&b, sizeof(b), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
          tic_tac_toe::PrintBoard(b);
          printf("\nWE WON! :)\n"); return false;
        }
        else
        {
          b.m_packetID = (unsigned char) tic_tac_toe::messages::ID_TTT_TURN_AND_BOARD;
          a_peer->Send((const char*)&b, sizeof(b), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
        }

        break;
      }

      case tic_tac_toe::messages::ID_TTT_OTHER_USER_WON:
      { 
        tic_tac_toe::Board b = *(tic_tac_toe::Board*)p->data;
        tic_tac_toe::PrintBoard(b);

        printf("\nWE LOST! :(\n"); return false;
      }

      default:
      { printf("\nReceived unhandled packet"); }
    }
  }

  return true;
}

int main(int argc, char** argv)
{
  RakNet::RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();

  // we assume that 2 arguments means we are trying to launch a server
  if (argc == 2)
  {
    const int portNum = atoi(argv[1]);
    if (LaunchServer(peer, portNum) == false)
    { return 0; }
  }
  else if (argc == 3 || argc == 4) // serverIP serverPort [clientPort]
  {
    const char* serverIP = argv[1];
    const int   serverPort = atoi(argv[2]);
    const int   clientPort = argc == 4 ? atoi(argv[3]) : serverPort + 1;

    if (LaunchClient(peer, serverIP, serverPort, clientPort) == false)
    { return 0; }
  }
  else
  {
    PrintUsage();
    return 0;
  }

  while (HandlePackets(peer))
  {
    RakSleep(30);
  }

  peer->Shutdown(300);
  RakNet::RakPeerInterface::DestroyInstance(peer);
}